#!/bin/bash

echo "===================================================="
echo "GLOBE DSG INFRASTRUCTURE"
echo "WELCOME! DSG INSTALLATION OF SEC-AGENTS"
echo "==================================================="
echo

#INSTANCE_IDINFO=$(cat ec2instanceid.txt)

if [ -f /etc/os-release ]; then
    . /etc/os-release
    OS=$NAME
    VER=$VERSION_ID
    ID_OS=$ID
    OS_VER="${VER:0:2}"
    OS_VER_AMZ="${VER:0:4}"
    echo $NAME "$OS_VER"

    if [ $ID == "centos" ] && [ $OS_VER -eq "7" ]
    then
        cd /tmp/secagents2021/gitlab-security-agents-2021/Installers/
        #Move and extract Splunk
        sudo tar -xvzf splunkforwarder-8.0.6-152fb4b2bb96-Linux-x86_64.tgz -C /opt/
        cd /opt/splunkforwarder/bin
        sudo ./splunk start --accept-license
        #Input Username and Password...how to automate.
        #admin
        #9<70j>SS
        sudo ./splunk enable boot-start 
        cd /opt/splunkforwarder/etc/system/local
        sudo cp /tmp/secagents2021/gitlab-security-agents-2021/Installers/SplunkInstaller-ISDP/deploymentclient.conf /opt/splunkforwarder/etc/system/local/
        sudo sed -i '7 i allowSslCompression = false' server.conf
        sudo sed -i '8 i sslVersions = tls' server.conf
        echo "Completed Configuring deployment.conf and server.conf.....Restarting the Splunk Agent"
        cd /opt/splunkforwarder/bin/
        sudo ./splunk restart
        sudo ./splunk status
        #2nd step of Splunk
        sudo cp -R /tmp/secagents2021/gitlab-security-agents-2021/Installers/SplunkInstaller-ISDP/Splunk2ndStep-ISDP/TA-isdp-linux-config /opt/splunkforwarder/etc/apps/
        sudo cp -R /tmp/secagents2021/gitlab-security-agents-2021/Installers/SplunkInstaller-ISDP/Splunk2ndStep-ISDP/TA-vpcendpoint-outputs /opt/splunkforwarder/etc/apps/
        cd /opt/splunkforwarder/etc/apps/TA-isdp-linux-config/bin/
        sudo chmod +x *.sh
        echo "Completed Configuring the 2nd Step for Splunk.....Restarting the Splunk Agent"
        cd /opt/splunkforwarder/bin/
        ./splunk restart
        ./splunk status
        cd /opt/splunkforwarder/etc/apps/TA-vpcendpoint-outputs/local/
        #Test to automate the editing of TA-endpoint outputs.conf
        echo "What is the Endpoint DNS Name:"
        read DNSname
        sed -i -e '$aserver = '$DNSname':9997' outputs.conf
        echo "server = $DNSname:9997"
        echo "Completed Configuring VPC Endpoint for Splunk.....Restarting the Splunk Agent"
        #restart again the splunk agent.
        cd /opt/splunkforwarder/bin/
        ./splunk restart
        sudo service splunk start
        ./splunk status
    fi


    if [ $ID == "ubuntu" ] && [ $OS_VER == "16" ]
    then
        cd /tmp/secagents2021/gitlab-security-agents-2021/Installers/
        #Move and extract Splunk
        sudo tar -xvzf splunkforwarder-8.0.6-152fb4b2bb96-Linux-x86_64.tgz -C /opt/
        cd /opt/splunkforwarder/bin
        sudo ./splunk start --accept-license
        #Input Username and Password...how to automate.
        #admin
        #9<70j>SS
        sudo ./splunk enable boot-start 
        cd /opt/splunkforwarder/etc/system/local
        sudo cp /tmp/secagents2021/gitlab-security-agents-2021/Installers/SplunkInstaller-ISDP/deploymentclient.conf /opt/splunkforwarder/etc/system/local/
        sudo sed -i '7 i allowSslCompression = false' server.conf
        sudo sed -i '8 i sslVersions = tls' server.conf
        echo "Completed Configuring deployment.conf and server.conf.....Restarting the Splunk Agent"
        cd /opt/splunkforwarder/bin/
        sudo ./splunk restart
        sudo ./splunk status
        #2nd step of Splunk
        sudo cp -R /tmp/secagents2021/gitlab-security-agents-2021/Installers/SplunkInstaller-ISDP/Splunk2ndStep-ISDP/TA-isdp-linux-config /opt/splunkforwarder/etc/apps/
        sudo cp -R /tmp/secagents2021/gitlab-security-agents-2021/Installers/SplunkInstaller-ISDP/Splunk2ndStep-ISDP/TA-vpcendpoint-outputs /opt/splunkforwarder/etc/apps/
        cd /opt/splunkforwarder/etc/apps/TA-isdp-linux-config/bin/
        sudo chmod +x *.sh
        echo "Completed Configuring the 2nd Step for Splunk.....Restarting the Splunk Agent"
        cd /opt/splunkforwarder/bin/
        ./splunk restart
        ./splunk status
        cd /opt/splunkforwarder/etc/apps/TA-vpcendpoint-outputs/local/
        #Test to automate the editing of TA-endpoint outputs.conf
        echo "What is the Endpoint DNS Name:"
        read DNSname
        sed -i -e '$aserver = '$DNSname':9997' outputs.conf
        echo "server = $DNSname:9997"
        echo "Completed Configuring VPC Endpoint for Splunk.....Restarting the Splunk Agent"
        #restart again the splunk agent.
        cd /opt/splunkforwarder/bin/
        ./splunk restart
        sudo service splunk start
        ./splunk status
    fi

       if [ $ID == "ubuntu" ] && [ $OS_VER == "18" ]
    then
        cd /tmp/secagents2021/gitlab-security-agents-2021/Installers/
        #Move and extract Splunk
        sudo tar -xvzf splunkforwarder-8.0.6-152fb4b2bb96-Linux-x86_64.tgz -C /opt/
        cd /opt/splunkforwarder/bin
        sudo ./splunk start --accept-license
        #Input Username and Password...how to automate.
        #admin
        #9<70j>SS
        sudo ./splunk enable boot-start 
        cd /opt/splunkforwarder/etc/system/local
        sudo cp /tmp/secagents2021/gitlab-security-agents-2021/Installers/SplunkInstaller-ISDP/deploymentclient.conf /opt/splunkforwarder/etc/system/local/
        sudo sed -i '7 i allowSslCompression = false' server.conf
        sudo sed -i '8 i sslVersions = tls' server.conf
        echo "Completed Configuring deployment.conf and server.conf.....Restarting the Splunk Agent"
        cd /opt/splunkforwarder/bin/
        sudo ./splunk restart
        sudo ./splunk status
        #2nd step of Splunk
        sudo cp -R /tmp/secagents2021/gitlab-security-agents-2021/Installers/SplunkInstaller-ISDP/Splunk2ndStep-ISDP/TA-isdp-linux-config /opt/splunkforwarder/etc/apps/
        sudo cp -R /tmp/secagents2021/gitlab-security-agents-2021/Installers/SplunkInstaller-ISDP/Splunk2ndStep-ISDP/TA-vpcendpoint-outputs /opt/splunkforwarder/etc/apps/
        cd /opt/splunkforwarder/etc/apps/TA-isdp-linux-config/bin/
        sudo chmod +x *.sh
        echo "Completed Configuring the 2nd Step for Splunk.....Restarting the Splunk Agent"
        cd /opt/splunkforwarder/bin/
        ./splunk restart
        ./splunk status
        cd /opt/splunkforwarder/etc/apps/TA-vpcendpoint-outputs/local/
        #Test to automate the editing of TA-endpoint outputs.conf
        echo "What is the Endpoint DNS Name:"
        read DNSname
        sed -i -e '$aserver = '$DNSname':9997' outputs.conf
        echo "server = $DNSname:9997"
        echo "Completed Configuring VPC Endpoint for Splunk.....Restarting the Splunk Agent"
        #restart again the splunk agent.
        cd /opt/splunkforwarder/bin/
        ./splunk restart
        sudo service splunk start
        ./splunk status
    fi

           if [ $ID == "ubuntu" ] && [ $OS_VER == "20" ]
    then
        cd /tmp/secagents2021/gitlab-security-agents-2021/Installers/
        #Move and extract Splunk
        sudo tar -xvzf splunkforwarder-8.0.6-152fb4b2bb96-Linux-x86_64.tgz -C /opt/
        cd /opt/splunkforwarder/bin
        sudo ./splunk start --accept-license
        #Input Username and Password...how to automate.
        #admin
        #9<70j>SS
        sudo ./splunk enable boot-start 
        cd /opt/splunkforwarder/etc/system/local
        sudo cp /tmp/secagents2021/gitlab-security-agents-2021/Installers/SplunkInstaller-ISDP/deploymentclient.conf /opt/splunkforwarder/etc/system/local/
        sudo sed -i '7 i allowSslCompression = false' server.conf
        sudo sed -i '8 i sslVersions = tls' server.conf
        echo "Completed Configuring deployment.conf and server.conf.....Restarting the Splunk Agent"
        cd /opt/splunkforwarder/bin/
        sudo ./splunk restart
        sudo ./splunk status
        #2nd step of Splunk
        sudo cp -R /tmp/secagents2021/gitlab-security-agents-2021/Installers/SplunkInstaller-ISDP/Splunk2ndStep-ISDP/TA-isdp-linux-config /opt/splunkforwarder/etc/apps/
        sudo cp -R /tmp/secagents2021/gitlab-security-agents-2021/Installers/SplunkInstaller-ISDP/Splunk2ndStep-ISDP/TA-vpcendpoint-outputs /opt/splunkforwarder/etc/apps/
        cd /opt/splunkforwarder/etc/apps/TA-isdp-linux-config/bin/
        sudo chmod +x *.sh
        echo "Completed Configuring the 2nd Step for Splunk.....Restarting the Splunk Agent"
        cd /opt/splunkforwarder/bin/
        ./splunk restart
        ./splunk status
        cd /opt/splunkforwarder/etc/apps/TA-vpcendpoint-outputs/local/
        #Test to automate the editing of TA-endpoint outputs.conf
        echo "What is the Endpoint DNS Name:"
        read DNSname
        sed -i -e '$aserver = '$DNSname':9997' outputs.conf
        echo "server = $DNSname:9997"
        echo "Completed Configuring VPC Endpoint for Splunk.....Restarting the Splunk Agent"
        #restart again the splunk agent.
        cd /opt/splunkforwarder/bin/
        ./splunk restart
        sudo service splunk start
        ./splunk status
    fi


    if [ $ID == "ubuntu" ] && [ $OS_VER == "14" ]
    then
        cd /tmp/secagents2021/gitlab-security-agents-2021/Installers/
        #Move and extract Splunk
        sudo tar -xvzf splunkforwarder-8.0.6-152fb4b2bb96-Linux-x86_64.tgz -C /opt/
        cd /opt/splunkforwarder/bin
        sudo ./splunk start --accept-license
        #Input Username and Password...how to automate.
        #admin
        #9<70j>SS
        sudo ./splunk enable boot-start 
        cd /opt/splunkforwarder/etc/system/local
        sudo cp /tmp/secagents2021/gitlab-security-agents-2021/Installers/SplunkInstaller-ISDP/deploymentclient.conf /opt/splunkforwarder/etc/system/local/
        sudo sed -i '7 i allowSslCompression = false' server.conf
        sudo sed -i '8 i sslVersions = tls' server.conf
        echo "Completed Configuring deployment.conf and server.conf.....Restarting the Splunk Agent"
        cd /opt/splunkforwarder/bin/
        sudo ./splunk restart
        sudo ./splunk status
        #2nd step of Splunk
        sudo cp -R /tmp/secagents2021/gitlab-security-agents-2021/Installers/SplunkInstaller-ISDP/Splunk2ndStep-ISDP/TA-isdp-linux-config /opt/splunkforwarder/etc/apps/
        sudo cp -R /tmp/secagents2021/gitlab-security-agents-2021/Installers/SplunkInstaller-ISDP/Splunk2ndStep-ISDP/TA-vpcendpoint-outputs /opt/splunkforwarder/etc/apps/
        cd /opt/splunkforwarder/etc/apps/TA-isdp-linux-config/bin/
        sudo chmod +x *.sh
        echo "Completed Configuring the 2nd Step for Splunk.....Restarting the Splunk Agent"
        cd /opt/splunkforwarder/bin/
        ./splunk restart
        ./splunk status
        cd /opt/splunkforwarder/etc/apps/TA-vpcendpoint-outputs/local/
        #Test to automate the editing of TA-endpoint outputs.conf
        echo "What is the Endpoint DNS Name:"
        read DNSname
        sed -i -e '$aserver = '$DNSname':9997' outputs.conf
        echo "server = $DNSname:9997"
        echo "Completed Configuring VPC Endpoint for Splunk.....Restarting the Splunk Agent"
        #restart again the splunk agent.
        cd /opt/splunkforwarder/bin/
        ./splunk restart
        sudo service splunk start
        ./splunk status
    fi

    if [ $ID == "amzn" ] && [ $OS_VER_AMZ == "2018" ]
    then
        cd /tmp/secagents2021/gitlab-security-agents-2021/Installers/
        #Move and extract Splunk
        sudo tar -xvzf splunkforwarder-8.0.6-152fb4b2bb96-Linux-x86_64.tgz -C /opt/
        cd /opt/splunkforwarder/bin
        sudo ./splunk start --accept-license
        #Input Username and Password...how to automate.
        #admin
        #9<70j>SS
        sudo ./splunk enable boot-start 
        cd /opt/splunkforwarder/etc/system/local
        sudo cp /tmp/secagents2021/gitlab-security-agents-2021/Installers/SplunkInstaller-ISDP/deploymentclient.conf /opt/splunkforwarder/etc/system/local/
        sudo sed -i '7 i allowSslCompression = false' server.conf
        sudo sed -i '8 i sslVersions = tls' server.conf
        echo "Completed Configuring deployment.conf and server.conf.....Restarting the Splunk Agent"
        cd /opt/splunkforwarder/bin/
        sudo ./splunk restart
        sudo ./splunk status
        #2nd step of Splunk
        sudo cp -R /tmp/secagents2021/gitlab-security-agents-2021/Installers/SplunkInstaller-ISDP/Splunk2ndStep-ISDP/TA-isdp-linux-config /opt/splunkforwarder/etc/apps/
        sudo cp -R /tmp/secagents2021/gitlab-security-agents-2021/Installers/SplunkInstaller-ISDP/Splunk2ndStep-ISDP/TA-vpcendpoint-outputs /opt/splunkforwarder/etc/apps/
        cd /opt/splunkforwarder/etc/apps/TA-isdp-linux-config/bin/
        sudo chmod +x *.sh
        echo "Completed Configuring the 2nd Step for Splunk.....Restarting the Splunk Agent"
        cd /opt/splunkforwarder/bin/
        ./splunk restart
        ./splunk status
        cd /opt/splunkforwarder/etc/apps/TA-vpcendpoint-outputs/local/
        #Test to automate the editing of TA-endpoint outputs.conf
        echo "What is the Endpoint DNS Name:"
        read DNSname
        sed -i -e '$aserver = '$DNSname':9997' outputs.conf
        echo "server = $DNSname:9997"
        echo "Completed Configuring VPC Endpoint for Splunk.....Restarting the Splunk Agent"
        #restart again the splunk agent.
        cd /opt/splunkforwarder/bin/
        ./splunk restart
        sudo service splunk start
        ./splunk status
    fi

    if [ $ID == "amzn" ] && [ $OS_VER_AMZ == "2015" ]
    then
        cd /tmp/secagents2021/gitlab-security-agents-2021/Installers/
        #Move and extract Splunk
        sudo tar -xvzf splunkforwarder-8.0.6-152fb4b2bb96-Linux-x86_64.tgz -C /opt/
        cd /opt/splunkforwarder/bin
        sudo ./splunk start --accept-license
        #Input Username and Password...how to automate.
        #admin
        #9<70j>SS
        sudo ./splunk enable boot-start 
        cd /opt/splunkforwarder/etc/system/local
        sudo cp /tmp/secagents2021/gitlab-security-agents-2021/Installers/SplunkInstaller-ISDP/deploymentclient.conf /opt/splunkforwarder/etc/system/local/
        sudo sed -i '7 i allowSslCompression = false' server.conf
        sudo sed -i '8 i sslVersions = tls' server.conf
        echo "Completed Configuring deployment.conf and server.conf.....Restarting the Splunk Agent"
        cd /opt/splunkforwarder/bin/
        sudo ./splunk restart
        sudo ./splunk status
        #2nd step of Splunk
        sudo cp -R /tmp/secagents2021/gitlab-security-agents-2021/Installers/SplunkInstaller-ISDP/Splunk2ndStep-ISDP/TA-isdp-linux-config /opt/splunkforwarder/etc/apps/
        sudo cp -R /tmp/secagents2021/gitlab-security-agents-2021/Installers/SplunkInstaller-ISDP/Splunk2ndStep-ISDP/TA-vpcendpoint-outputs /opt/splunkforwarder/etc/apps/
        cd /opt/splunkforwarder/etc/apps/TA-isdp-linux-config/bin/
        sudo chmod +x *.sh
        echo "Completed Configuring the 2nd Step for Splunk.....Restarting the Splunk Agent"
        cd /opt/splunkforwarder/bin/
        ./splunk restart
        ./splunk status
        cd /opt/splunkforwarder/etc/apps/TA-vpcendpoint-outputs/local/
        #Test to automate the editing of TA-endpoint outputs.conf
        echo "What is the Endpoint DNS Name:"
        read DNSname
        sed -i -e '$aserver = '$DNSname':9997' outputs.conf
        echo "server = $DNSname:9997"
        echo "Completed Configuring VPC Endpoint for Splunk.....Restarting the Splunk Agent"
        #restart again the splunk agent.
        cd /opt/splunkforwarder/bin/
        ./splunk restart
        sudo service splunk start
        ./splunk status
    fi

    if [ $ID == "amzn" ] && [ $OS_VER_AMZ == "2017" ]
    then
        cd /tmp/secagents2021/gitlab-security-agents-2021/Installers/
        #Move and extract Splunk
        sudo tar -xvzf splunkforwarder-8.0.6-152fb4b2bb96-Linux-x86_64.tgz -C /opt/
        cd /opt/splunkforwarder/bin
        sudo ./splunk start --accept-license
        #Input Username and Password...how to automate.
        #admin
        #9<70j>SS
        sudo ./splunk enable boot-start 
        cd /opt/splunkforwarder/etc/system/local
        sudo cp /tmp/secagents2021/gitlab-security-agents-2021/Installers/SplunkInstaller-ISDP/deploymentclient.conf /opt/splunkforwarder/etc/system/local/
        sudo sed -i '7 i allowSslCompression = false' server.conf
        sudo sed -i '8 i sslVersions = tls' server.conf
        echo "Completed Configuring deployment.conf and server.conf.....Restarting the Splunk Agent"
        cd /opt/splunkforwarder/bin/
        sudo ./splunk restart
        sudo ./splunk status
        #2nd step of Splunk
        sudo cp -R /tmp/secagents2021/gitlab-security-agents-2021/Installers/SplunkInstaller-ISDP/Splunk2ndStep-ISDP/TA-isdp-linux-config /opt/splunkforwarder/etc/apps/
        sudo cp -R /tmp/secagents2021/gitlab-security-agents-2021/Installers/SplunkInstaller-ISDP/Splunk2ndStep-ISDP/TA-vpcendpoint-outputs /opt/splunkforwarder/etc/apps/
        cd /opt/splunkforwarder/etc/apps/TA-isdp-linux-config/bin/
        sudo chmod +x *.sh
        echo "Completed Configuring the 2nd Step for Splunk.....Restarting the Splunk Agent"
        cd /opt/splunkforwarder/bin/
        ./splunk restart
        ./splunk status
        cd /opt/splunkforwarder/etc/apps/TA-vpcendpoint-outputs/local/
        #Test to automate the editing of TA-endpoint outputs.conf
        echo "What is the Endpoint DNS Name:"
        read DNSname
        sed -i -e '$aserver = '$DNSname':9997' outputs.conf
        echo "server = $DNSname:9997"
        echo "Completed Configuring VPC Endpoint for Splunk.....Restarting the Splunk Agent"
        #restart again the splunk agent.
        cd /opt/splunkforwarder/bin/
        ./splunk restart
        sudo service splunk start
        ./splunk status
    fi

    if [ $ID == "amzn" ] && [ $OS_VER == "2" ]
    then
        cd /tmp/secagents2021/gitlab-security-agents-2021/Installers/
        #Move and extract Splunk
        sudo tar -xvzf splunkforwarder-8.0.6-152fb4b2bb96-Linux-x86_64.tgz -C /opt/
        cd /opt/splunkforwarder/bin
        sudo ./splunk start --accept-license
        #Input Username and Password...how to automate.
        #admin
        #9<70j>SS
        /opt/splunkforwarder/bin/splunk start
        sudo ./splunk enable boot-start 
        cd /opt/splunkforwarder/etc/system/local
        sudo cp /tmp/secagents2021/gitlab-security-agents-2021/Installers/SplunkInstaller-ISDP/deploymentclient.conf /opt/splunkforwarder/etc/system/local/
        sudo sed -i '7 i allowSslCompression = false' server.conf
        sudo sed -i '8 i sslVersions = tls' server.conf
        echo "Completed Configuring deployment.conf and server.conf.....Restarting the Splunk Agent"
        cd /opt/splunkforwarder/bin/
        sudo ./splunk restart
        sudo ./splunk status
        #2nd step of Splunk
        sudo cp -R /tmp/secagents2021/gitlab-security-agents-2021/Installers/SplunkInstaller-ISDP/Splunk2ndStep-ISDP/TA-isdp-linux-config /opt/splunkforwarder/etc/apps/
        sudo cp -R /tmp/secagents2021/gitlab-security-agents-2021/Installers/SplunkInstaller-ISDP/Splunk2ndStep-ISDP/TA-vpcendpoint-outputs /opt/splunkforwarder/etc/apps/
        cd /opt/splunkforwarder/etc/apps/TA-isdp-linux-config/bin/
        sudo chmod +x *.sh
        echo "Completed Configuring the 2nd Step for Splunk.....Restarting the Splunk Agent"
        cd /opt/splunkforwarder/bin/
        ./splunk restart
        ./splunk status
        cd /opt/splunkforwarder/etc/apps/TA-vpcendpoint-outputs/local/
        #Test to automate the editing of TA-endpoint outputs.conf
        echo "What is the Endpoint DNS Name:"
        read DNSname
        sed -i -e '$aserver = '$DNSname':9997' outputs.conf
        echo "server = $DNSname:9997"
        echo "Completed Configuring VPC Endpoint for Splunk.....Restarting the Splunk Agent"
        #restart again the splunk agent.
        cd /opt/splunkforwarder/bin/
        ./splunk restart
        sudo service splunk start
        ./splunk status
    fi


    if [ $ID == "rhel" ] && [ $OS_VER == "7" ]
    then
        cd /tmp/secagents2021/gitlab-security-agents-2021/Installers/
        #Move and extract Splunk
        sudo tar -xvzf splunkforwarder-8.0.6-152fb4b2bb96-Linux-x86_64.tgz -C /opt/
        cd /opt/splunkforwarder/bin
        sudo ./splunk start --accept-license
        #Input Username and Password...how to automate.
        #admin
        #9<70j>SS
        sudo ./splunk enable boot-start 
        cd /opt/splunkforwarder/etc/system/local
        sudo cp /tmp/secagents2021/gitlab-security-agents-2021/Installers/SplunkInstaller-ISDP/deploymentclient.conf /opt/splunkforwarder/etc/system/local/
        sudo sed -i '7 i allowSslCompression = false' server.conf
        sudo sed -i '8 i sslVersions = tls' server.conf
        echo "Completed Configuring deployment.conf and server.conf.....Restarting the Splunk Agent"
        cd /opt/splunkforwarder/bin/
        sudo ./splunk restart
        sudo ./splunk status
        #2nd step of Splunk
        sudo cp -R /tmp/secagents2021/gitlab-security-agents-2021/Installers/SplunkInstaller-ISDP/Splunk2ndStep-ISDP/TA-isdp-linux-config /opt/splunkforwarder/etc/apps/
        sudo cp -R /tmp/secagents2021/gitlab-security-agents-2021/Installers/SplunkInstaller-ISDP/Splunk2ndStep-ISDP/TA-vpcendpoint-outputs /opt/splunkforwarder/etc/apps/
        cd /opt/splunkforwarder/etc/apps/TA-isdp-linux-config/bin/
        sudo chmod +x *.sh
        echo "Completed Configuring the 2nd Step for Splunk.....Restarting the Splunk Agent"
        cd /opt/splunkforwarder/bin/
        ./splunk restart
        ./splunk status
        cd /opt/splunkforwarder/etc/apps/TA-vpcendpoint-outputs/local/
        #Test to automate the editing of TA-endpoint outputs.conf
        echo "What is the Endpoint DNS Name:"
        read DNSname
        sed -i -e '$aserver = '$DNSname':9997' outputs.conf
        echo "server = $DNSname:9997"
        echo "Completed Configuring VPC Endpoint for Splunk.....Restarting the Splunk Agent"
        #restart again the splunk agent.
        cd /opt/splunkforwarder/bin/
        ./splunk restart
        sudo service splunk start
        ./splunk status
    fi

fi

    echo
    echo "Completed..."

 