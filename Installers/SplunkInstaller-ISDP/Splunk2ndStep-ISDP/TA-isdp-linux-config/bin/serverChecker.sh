#!/bin/bash
#######################################
### CREATED BY: R2C                 ###
### DATE CREATED: 2019-06-06        ###
### MODIFIED BY: R2C                ###
### DATE MODIFIED: 2020-04-22       ###
#######################################


#######################################
#####   DO NOT TOUCH THIS PART   ######
#######################################

function currentDT () {
        dt=$(date '+%D %H:%M:%S');
        printf "$dt"
}


function serverChecker () {

        mb[1]="/sbin/update";
        mb[2]="/usr/bin/logwatcher";
        mb[3]="/usr/bin/kmodcheckd";
        mb[4]="/usr/lib/systemd/system/randomx.service";
        mb[5]="/lib/null";
        mb[6]="/usr/bin/mlx";
        mb[7]="/usr/lib/eth-scsi/libethscsi.so";
        mb[8]="/usr/lib/liba2ps.la";
        mb[9]="/usr/sbin/bashrc";
        mb[10]="/var/lib/masses.bo";
        mb[11]="/usr/lib/mess3.bo";
        mb[12]="/usr/lib/mess.bo";
        mb[13]="/usr/sbin/gpasswd";
        mb[14]="/bin/taskme";
        mb[15]="/tmp/xxx";
        mb[16]="/tmp/x";
        mb[17]="/lib/morse.ko";
        mb[18]="/usr/bin/ssh2";
        mb[19]="/usr/lib/libidcm.so.l";
        mb[20]="/usr/sbin/lmx";
        mb[21]="/usr/bin/rwho.cups";
        mb[22]="/sbin/yellow";
        mb[23]="/usr/bin/lmx";
        mb[24]="/usr/sbin/bashrcd";
        mb[25]="/usr/sbin/mars";
        mb[26]="/sbin/mars";
        mb[27]="/usr/sbin/nebula";
        mb[28]="/usr/lib/mebula";
        mb[29]="/usr/bin/melt";
        mb[30]="/tmp/a";
        mb[31]="/tmp/a.c";
        mb[32]="/dev/shm/a";
        mb[33]="/usr/bin/malloc";
        mb[34]="/usr/bin/sperl";
        mb[35]="/usr/sbin/land";
        mb[36]="/usr/bin/looked";
        mb[37]="/tmp/.ICE-unix/sss-3.10.0-327.13.1.el7.x86_64#1SMP_openssh-6.6p1.tar.bz2";
        mb[38]="/tmp/.ICE-unix/usr/share/zoneinfo/right/Pacific/help/ssh_";
        mb[39]="/tmp/.ICE-unix/usr/share/zoneinfo/right/Pacific/help/sshd_";
        mb[40]="/tmp/.ICE-unix/usr/share/zoneinfo/right/Pacific/help/ssh";
        mb[41]="/tmp/.ICE-unix/usr/share/zoneinfo/right/Pacific/help/sshd";
        mb[42]="/tmp/.ICE-unix/usr/share/zoneinfo/right/Pacific/help/.Sd";
        mb[43]="/tmp/.ICE-unix/usr/share/zoneinfo/right/Pacific/help/.Sh";
        mb[44]="/tmp/.ICE-unix/usr/share/zoneinfo/right/Pacific/help/nt";
        mb[45]="/tmp/.ICE-unix/usr/share/zoneinfo/right/Pacific/help/i1";
        mb[46]="/tmp/.ICE-unix/usr/share/zoneinfo/right/Pacific/help/i2";
        mb[47]="/usr/share/yelp/xslt/help/ssh_";
        mb[48]="/usr/share/yelp/xslt/help/sshd_";
        mb[49]="/usr/share/yelp/xslt/help/ssh";
        mb[50]="/usr/share/yelp/xslt/help/sshd";
        mb[51]="/usr/share/yelp/xslt/help/.Sd";
        mb[52]="/usr/share/yelp/xslt/help/.Sh";
        mb[53]="/usr/share/yelp/xslt/help/nt";
        mb[54]="/usr/share/yelp/xslt/help/i1";
        mb[55]="/usr/share/yelp/xslt/help/i2";


    sbin_update_action="Check /sbin/update in the memory."
    sbin_update_ls_res=`ls -l /proc/*/exe 2> /dev/null | egrep /sbin/update$`
    sbin_update=`ls -l /proc/*/exe 2> /dev/null | egrep /sbin/update$ | wc -l`
    if [ $sbin_update == "0" ]; then
        echo "$(currentDT) $sbin_update_action Malware file /sbin/update is not present in the memory."
    else
        echo "$(currentDT) $sbin_update_action Malware file /sbin/update is running in the memory. $sbin_update_ls_res"
    fi

    malware_action="Check malware's perl process."
    malware_ls_res=`ls -l /proc/*/exe 2> /dev/null | grep perl`;
    perl_pids=`ls -l /proc/*/exe 2> /dev/null | grep perl | awk -F/ '{print $3}'`


    found_malware_perl="0"
        if [ -n "$perl_pids" ]; then
                for ((i=0;i<${#perl_pids[@]};i++)); do
                    if [ "${perl_pids[$i]}" != '' ]; then
                        ps_output=`ps -p ${perl_pids[$i]} | wc -l`
                        if [ $ps_output == "1" ]; then
                            found_malware_perl="1"
                        fi
                    fi
                done
        fi

    if [ $found_malware_perl == "1" ]; then
        echo "$(currentDT) $malware_action Malware perl file is running in the memory. $malware_ls_res"
    else
        echo "$(currentDT) $malware_action Malware perl file is not present in the memory."
    fi


    libeth_action="Check keylogger activity (libethscsci.so file)."
    if [ -f /usr/lib/eth-scsi/libethscsi.so ]; then
        libeth_path=`ls -l /usr/lib/eth-scsi/libethscsi.so`
        libeth_lines=`ls -l /usr/lib/eth-scsi/libethscsi.so | sort -u | wc -l`
        libeth_date=`ls -l /usr/lib/eth-scsi/libethscsi.so --time-style="+%Y-%m-%d %H:%M" | awk '{print $6, $7}'`

        libeth_res="$libeth_path, $libeth_lines password(s) were captured by the malware, Last time password was captured was at $libeth_date."

        echo "$(currentDT) $libeth_action $libeth_res"
    else
        echo "$(currentDT) $libeth_action No passwords were captured."
    fi


    ls_action="Check ls command."
    touch /tmp/.mailtmp
    ls_results=`ls -l /tmp/.mailtmp | wc -l`
    if [ $ls_results == "0" ]; then
        echo "$(currentDT) $ls_action ls is compromised."
    else
        echo "$(currentDT) $ls_action ls is clean."
    fi
    rm -f /tmp/.mailtmp

    ml_strup_action="Check malware /sbin/update if configured in the startup."
    startup_is_compromised=0
    if [ -d "/etc/init.d/" ]; then

        startup=`grep " /sbin/update " /etc/init.d/* | grep -v "#.*/sbin/update" | wc -l`
        grep_res=`grep " /sbin/update " /etc/init.d/* | egrep -v "^#"`

        if [ $startup != "0" ]; then
            startup_is_compromised=1
        fi
    fi

    if [ -d "/etc/rc.d/init.d/" ]; then
        startup=`grep " /sbin/update " /etc/rc.d/init.d/* | grep -v "#.*/sbin/update" | wc -l`
        grep_res=`grep " /sbin/update " /etc/rc.d/init.d/* | egrep -v "^#"`
        if [ $startup != "0" ]; then
            startup_is_compromised=1
        fi
    fi

    if [ $startup_is_compromised == 1 ]; then
        echo "$(currentDT) $ml_strup_action Startup scripts are compromised. $grep_res"
    else
        echo "$(currentDT) $ml_strup_action Startup scripts are clean."
    fi

    pscan_action="Check pnscan if installed."
    if [ -f /usr/bin/pnscan ]; then
        pnscan_ls_res=`ls -l /usr/bin/pnscan`
        echo "$(currentDT) $pscan_action pnscan is installed on the server. $pnscan_ls_res"
    else
        echo "$(currentDT) $pscan_action pnscan is not is installed on the server."
    fi


    nmap_action="Check nmap if installed."

    nmap=`rpm -q nmap --qf '%{NAME}-%{VERSION}-%{RELEASE}.%{ARCH} was installed on %{INSTALLTIME:date}\n' | grep -v 'is not installed' | wc -l`
    if [ $nmap == "0" ]; then
        echo "$(currentDT) $nmap_action nmap is not installed on the server."
    else
        nmap_ch_res=`rpm -q nmap --qf '%{NAME}-%{VERSION}-%{RELEASE}.%{ARCH} was installed on %{INSTALLTIME:date}'`
        echo "$(currentDT) $nmap_action $nmap_ch_res."
        #echo ----- nmap, a network scanner tool is installed on the server.
        #echo This tool is being used by the attacker.
        #echo It might be install by him or it was installed already in the server.
    fi


    xz_action="Check xz if installed."

    xz=`rpm -q xz --qf '%{NAME}-%{VERSION}-%{RELEASE}.%{ARCH} was installed on %{INSTALLTIME:date}\n' | grep -v 'is not installed' | wc -l`
    if [ $xz == "0" ]; then
        echo "$(currentDT) $xz_action xz is not installed on the server."
    else
        xz_ch_res=`rpm -q xz --qf '%{NAME}-%{VERSION}-%{RELEASE}.%{ARCH} was installed on %{INSTALLTIME:date}'`
        echo "$(currentDT) $xz_action $xz_ch_res, legit package but less common, Need to verify it."
        #echo ----- xz is installed on the server
        #echo xz is a legit package but less common, which is being also installed by the malware is installed on the server.
        #echo It might be install by him or it was installed already in the server. Need to verify it.
    fi


        ssh_conf_action="Checking /etc/ssh/sshd_config for Root Login"
    ssh_conf_chk=`cat /etc/ssh/sshd_config | grep PermitRootLogin`


    if [[ -z $ssh_conf_chk ]]; then
        echo "$(currentDT) $ssh_conf_action not exist."
    else
        echo "$(currentDT) $ssh_conf_action. $ssh_conf_chk"
    fi


    for((cnt=1; cnt<=${#mb[@]}; cnt++));
    do
        if [ -f ${mb[$cnt]} ]; then
            echo "$(currentDT) Checking ${mb[$cnt]} if exist. Malicious Binary Existing!"
        else
            echo "$(currentDT) Checking ${mb[$cnt]} if exist. File not found "
        fi
    done
}

my_filename=$(basename -- "$0")

#CHECK IF THE SCRIPT STILL RUNNING THEN KILL IT
checkIfRunning=$(ps -eo lstart,pid,cmd --sort=start_time | grep -m 2 "$my_filename" | grep -v "grep"  | awk '{ cmd="date -d\""$1 FS $2 FS $3 FS $4 FS $5"\" +\047%s\047"; cmd | getline d; close(clearcmd); $1=$2=$3=$4=$5=""; printf "%s\n",
d$0 }')

arrayRun=($checkIfRunning)
datetime=${arrayRun[0]}
procid=${arrayRun[1]}
exetype=${arrayRun[2]}
flname=${arrayRun[3]}
cdatetime=$(date '+%s')
datetimeplusone=$(($datetime + 1))

if [ "$datetimeplusone" -lt "$cdatetime" ]; then
    killMe=$(kill -9 "$procid" 2>/dev/null)
    killMeStatus=$?
    if [ "$killMeStatus" == "0" ]; then
        dt=$(date '+%Y-%m-%d %H:%M:%S');
        #echo "$dt $my_filename has been successfully stopped and Start."
        serverChecker # CALL FUNCTION #
    else
        dt=$(date '+%Y-%m-%d %H:%M:%S');
        echo "$dt $my_filename unable to stop. Error: $killMeStatus"
    fi
else
    serverChecker # CALL FUNCTION #
fi

