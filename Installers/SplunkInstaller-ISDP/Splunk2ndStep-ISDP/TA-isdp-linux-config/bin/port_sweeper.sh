#!/bin/bash
#######################################
### CREATED BY: R2C                 ###
### DATE CREATED: 2020-01-20        ###
### MODIFIED BY: R2C                ###
### DATE MODIFIED: 2020-01-20       ###
#######################################

target_file="/etc/ssh/sshd_config";
target_string="Port";

#######################################
#####   DO NOT TOUCH THIS PART   ######
#######################################

function main () {
        filepath="$1";
        targetval="$2";
        current_datetime=$(date '+%m/%d/%Y %H:%M:%S');

        if [ -f "$filepath" ]; then
                modified_datetime=`date -r $filepath '+%m/%d/%Y %H:%M:%S'`;
                resContent=`grep -in -w "^$targetval" "$filepath" | awk -F: '{print $2}' | sed -e 's/^Port[[:space:]]*//'`;

                SAVEIFS=$IFS;   # Save current IFS
                IFS=$',|\n';      # Change IFS to New Line & Comma ('\n' and ',')
                arrResContent=($resContent); # split to array $resContent
                IFS=$SAVEIFS;   # Restore IFS

                if [ -n "$arrResContent" ]; then
                        for (( i=0; i<${#arrResContent[@]}; i++ ))
                        do
                                echo "$current_datetime | $targetval ${arrResContent[$i]} | modtime [$modified_datetime]";
                        done
                else
                        echo "$current_datetime | No Port Defined. | modtime [$modified_datetime]";
                fi
        else
                echo "$current_datetime | $filepath Not Found. | modtime [N/A]";
        fi
}

main $target_file $target_string # CALL MAIN FUNCTION #


