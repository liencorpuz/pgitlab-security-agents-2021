#!/bin/bash
#######################################
### CREATED BY: R2C                             ###
### DATE CREATED: 2019-05-28        ###
### MODIFIED BY: R2C                    ###
### DATE MODIFIED: 2019-05-30       ###
### FOR SSH KEYS CHECKING                   ###
#######################################

fsource_path="/opt/splunkforwarder/etc/apps/TA-isdp-linux-config/bin/keylist.txt"

#######################################
#####   DO NOT TOUCH THIS PART   ######
#######################################
function main () {
        #sleep 60s
        source_file="$1"
        scanFinalResults=''
        #While loop to read line by line
        while
                read -r textline
        do
                set -f # AVOID GLOBBING (EXPANSION OF *).
                array=(${textline//|/ })
                path_val=${array[0]} #Targetpath
                pkey_val=${array[1]} #Targetkey
                if [ "$path_val" == "/home/" ]; then
                        homedirs=`ls -l $path_val | egrep '^d' | awk '{print $9}'`
                        scanResults=''
                        for homedir in $homedirs
                        do
                                OIFS=$IFS;
                                IFS="|";
                                sshpathdir="$path_val${homedir}/"
                                sshpath="$path_val${homedir}/.ssh/"
                                if [ -d "$sshpath" ]; then

                                        otherfiles=`ls "$sshpath" | awk 'BEGIN{RS="\n";ORS="|"}{print}'`
                                        scanRawResults=''
                                        scanRawCount=0
                                        for otherfile in $otherfiles
                                        do
                                                checktxtfile=`file -b --mime-type "$sshpath${otherfile}" | sed 's|/.*||'`
                                                if [ $checktxtfile == "text" ]; then
                                                        txtfullpath=$sshpath${otherfile}
                                                        resPath=`grep -wrl "$pkey_val" "$txtfullpath" 2>&1 | grep -v 'Permission denied\|Binary\|No such file or directory\|\x00'`
                                                        if [ -n "$resPath" ]; then
                                                                dt=$(date '+%D %H:%M:%S');
                                                                res_hash=`sha256sum $resPath | cut -d' ' -f1`
                                                                scanRawResults+="$dt $resPath $res_hash \n"
                                                                ((scanRawCount++))
                                                        fi
                                                fi
                                        done
                                        if [ $scanRawCount -gt 0 ]; then
                                                scanFinalResults+="$scanRawResults"
                                        else
                                                dt=$(date '+%D %H:%M:%S');
                                                scanFinalResults+="$dt $sshpath No keys found. \n"
                                        fi
                                else
                                        dt=$(date '+%D %H:%M:%S');
                                        scanResults+="$dt $sshpathdir .ssh Folder not exist. \n"
                                fi
                                IFS=$OIFS;
                        done
                        scanFinalResults+="$scanResults"
                else

                        if [ -d "$path_val" ]; then

                                otherfiles=`ls "$path_val" | awk 'BEGIN{RS="\n";ORS="|"}{print}'`
                                OIFS=$IFS;
                                IFS="|";
                                scanRawResults=''
                                scanRawCount=0
                                for otherfile in $otherfiles
                                do
                                        checktxtfile=`file -b --mime-type "$path_val${otherfile}" | sed 's|/.*||'`
                                        if [ $checktxtfile == "text" ]; then
                                                txtfullpath=$path_val${otherfile}
                                                resPath=`grep -wrl "$pkey_val" "$txtfullpath" 2>&1 | grep -v 'Permission denied\|Binary\|No such file or directory\|\x00'`
                                                if [ -n "$resPath" ]; then
                                                        dt=$(date '+%D %H:%M:%S');
                                                        #res_hash=$(eval "sha256sum $resPath | cut -d' ' -f1")
                                                        res_hash=`sha256sum $resPath | cut -d' ' -f1`
                                                        scanRawResults+="$dt $resPath $res_hash \n"
                                                        ((scanRawCount++))
                                                fi
                                        fi
                                done
                                IFS=$OIFS;

                                if [ $scanRawCount -gt 0 ]; then
                                        scanFinalResults+="$scanRawResults"
                                else
                                        dt=$(date '+%D %H:%M:%S');
                                        scanFinalResults+="$dt $path_val No keys found. \n"
                                fi
                        else
                                dt=$(date '+%D %H:%M:%S');
                                scanFinalResults+="$dt $path_val Folder not exist. \n"
                        fi
                fi

        done < "$source_file"
        printf "$scanFinalResults"
}

my_filename=$(basename -- "$0")

#CHECK IF THE SCRIPT STILL RUNNING THEN KILL IT
checkIfRunning=`ps -eo lstart,pid,cmd --sort=start_time | grep -m 2 "$my_filename" | grep -v "grep"  | awk '{ cmd="date -d\""$1 FS $2 FS $3 FS $4 FS $5"\" +\047%s\047"; cmd | getline d; close(clearcmd); $1=$2=$3=$4=$5=""; printf "%s\n",d$0 }'`

arrayRun=($checkIfRunning)
datetime=${arrayRun[0]}
procid=${arrayRun[1]}
exetype=${arrayRun[2]}
flname=${arrayRun[3]}
cdatetime=$(date '+%s')
datetimeplusone=$(($datetime + 1))

if [ "$datetimeplusone" -lt "$cdatetime" ]; then

        killMe=$(kill -9 "$procid" 2>/dev/null)
        killMeStatus=$?
        if [ "$killMeStatus" == "0" ]; then
                dt=$(date '+%Y-%m-%d %H:%M:%S');
                #echo "$dt $my_filename has been successfully stopped and Start."
                main $fsource_path # CALL MAIN FUNCTION #
        else
                dt=$(date '+%Y-%m-%d %H:%M:%S');
                echo "$dt $my_filename unable to stop. Error: $killMeStatus"
        fi
else
        main $fsource_path # CALL MAIN FUNCTION #
fi


